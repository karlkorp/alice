# ALICE O<sup>2</sup>
ALICE (A Large Ion Collider Experiment) is a general purpose,
heavy ion collision detector at the CERN LHC.
It is designed to study the physics of strongly interacting matter,
and in particular the properties of Quark-Gluon Plasma (QGP),
using proton-proton, nucleus-nucleus and proton-nucleus collisions at high energies.
The ALICE experiment will be upgraded during the Long Shutdown 2 (LS2, 2020-2021)
in order to exploit the full scientific potential of the future LHC.

## Useful links
- [ALICE Analysis Tutorial documentation](https://alice-doc.github.io/alice-analysis-tutorial/)
- [ALICE O<sup>2</sup> Analysis Framework](https://aliceo2group.github.io/analysis-framework/)
- [ALICE O<sup>2</sup> Project](https://alice-o2-project.web.cern.ch/)
- [ALICE O<sup>2</sup> Software on GitHub](https://aliceo2group.github.io/)

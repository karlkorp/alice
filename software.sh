sudo apt update  -y
sudo apt upgrade -y

sudo apt install -y autoconf
sudo apt install -y automake
sudo apt install -y autopoint
sudo apt install -y binutils
sudo apt install -y bison
sudo apt install -y build-essential
sudo apt install -y clang
sudo apt install -y clang-format
sudo apt install -y clang-tidy
sudo apt install -y clang-tools
sudo apt install -y clangd
sudo apt install -y cmake
sudo apt install -y cmake-curses-gui
sudo apt install -y coreutils
sudo apt install -y curl
sudo apt install -y emacs
sudo apt install -y environment-modules
sudo apt install -y ffmpeg
sudo apt install -y fftw-dev
sudo apt install -y flex
sudo apt install -y fonts-cascadia-code
sudo apt install -y freeglut3-dev
sudo apt install -y g++
sudo apt install -y gcc
sudo apt install -y gdb
sudo apt install -y gettext
sudo apt install -y gfortran
sudo apt install -y git
sudo apt install -y gnuplot
sudo apt install -y htop
sudo apt install -y libasio-dev
sudo apt install -y libbenchmark-dev
sudo apt install -y libboost-all-dev
sudo apt install -y libbz2-dev
sudo apt install -y libc++-dev
sudo apt install -y libc++abi-dev
sudo apt install -y libcurl4-gnutls-dev
sudo apt install -y libeigen3-dev
sudo apt install -y libexpat1-dev
sudo apt install -y libfftw3-dev
sudo apt install -y libflatbuffers-dev
sudo apt install -y libglfw3-dev
sudo apt install -y libglu1-mesa-dev
sudo apt install -y libgmp-dev
sudo apt install -y libgsl-dev
sudo apt install -y libgtest-dev
sudo apt install -y libjsoncpp-dev
sudo apt install -y liblapack-dev
sudo apt install -y liblapacke-dev
sudo apt install -y liblzma-dev
sudo apt install -y libmsgsl-dev
sudo apt install -y libmysqlclient-dev
sudo apt install -y libnanomsg-dev
sudo apt install -y libncurses-dev
sudo apt install -y libperl-dev
sudo apt install -y libtbb-dev
sudo apt install -y libtool
sudo apt install -y libtool-bin
sudo apt install -y libxerces-c-dev
sudo apt install -y libxml2-dev
sudo apt install -y libz3-dev
sudo apt install -y lld
sudo apt install -y lldb
sudo apt install -y llvm
sudo apt install -y lsb-release
sudo apt install -y make
sudo apt install -y meson
sudo apt install -y musl
sudo apt install -y neovim
sudo apt install -y ninja-build
sudo apt install -y nlohmann-json3-dev
sudo apt install -y pkg-config
sudo apt install -y python3-venv
sudo apt install -y rsync
sudo apt install -y software-properties-common
sudo apt install -y strace
sudo apt install -y swig
sudo apt install -y texinfo
sudo apt install -y unzip
sudo apt install -y valgrind
sudo apt install -y vc-dev
sudo apt install -y xorg-dev

sudo add-apt-repository -y ppa:alisw/ppa
sudo apt update         -y
sudo apt install        -y python3-alibuild

sudo apt autoclean  -y
sudo apt autoremove -y
sudo apt clean      -y

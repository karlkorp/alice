mkdir -p CERN
cd CERN || return

aliBuild init O2@dev           --defaults o2
aliBuild init O2Physics@master --defaults o2

aliDoctor O2        --defaults o2
aliDoctor O2Physics --defaults o2

read -r -p "Press 'enter' to continue..."

aliBuild build O2        --defaults o2
aliBuild build O2Physics --defaults o2
